package com.hcl.admin.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class LoginUser {

	@Id
	private String emailId;
	private String name;
	private String password;
	private String type;

	public LoginUser() {
		System.out.println("Login User Class Constructor");
	}

	public LoginUser(String emailId, String name, String password,String type) {
		super();
		this.emailId = emailId;
		this.name = name;
		this.password = password;
		this.type = type;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "LoginUser [emailId=" + emailId + ", name=" + name + ", password=" + password + ", type=" + type + "]";
	}
}
