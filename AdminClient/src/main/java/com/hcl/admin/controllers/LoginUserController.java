package com.hcl.admin.controllers;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.hcl.admin.bean.LoginUser;
import com.hcl.admin.service.ILoginUserService;

@Controller
public class LoginUserController {

	@Autowired
	private ILoginUserService loginUserService;

	@PostMapping("/getRegistered")
	public String getUserRegistered(LoginUser loginUser,HttpSession httpSession) {

		System.out.println("getRegisteredDashboardPage is called with " + loginUser);

		if(this.loginUserService.registerUser(loginUser)) {
			System.out.println(this.loginUserService.registerUser(loginUser));
			System.out.println("User is logged in successfully");
			httpSession.setAttribute("msg", "Your Are Registered Successfully Please Login");
			return "redirect:/message";
		}else {
			httpSession.setAttribute("msg", "You are Already Registered");
			return "redirect:/message";
		}
	}

	@GetMapping("/login")
	public String getLoginPage() {
		System.out.println("getLoginPage() is called");
		return "login";
	}

	@PostMapping("/dashboard")
	public String getDashboard(LoginUser loginUser,HttpSession session) {
		System.out.println("getDashboard() is called");

		LoginUser user;
		try {
			user = this.loginUserService.findUser(loginUser);

			if(user.getEmailId().equals(loginUser.getEmailId()) && user.getPassword().equals(loginUser.getPassword())) {
				session.setAttribute("email",user.getEmailId());
				session.setAttribute("name", user.getName());
				session.setAttribute("type", user.getType());
				return "dashboard";
			}else {
				System.out.println("not a valid Password");
				session.setAttribute("msg", "password is not valid");
				return "redirect:/message";
			}
		} catch (Exception exception) {
			exception.printStackTrace();
			session.setAttribute("msg", "Seems You are new User Please Register");
			return "redirect:/message";
		}
	}

	@GetMapping("/register")
	public String getRegisterPage() {
		System.out.println("getRegisterPage() is called");
		return "register";
	}

	@GetMapping("/logout")
	public String logout(HttpSession httpSession) {
		System.out.println("LoginUser logout() is called");

		httpSession.removeAttribute("email");
		httpSession.removeAttribute("name");
		httpSession.removeAttribute("type");
		httpSession.invalidate();

		return "redirect:/login";
	}

	@GetMapping("/dashboard")
	public String getDashboard() {
		System.out.println("getRegisterPage() is called");
		return "dashboard";
	}
}
