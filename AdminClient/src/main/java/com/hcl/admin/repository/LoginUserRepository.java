package com.hcl.admin.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.hcl.admin.bean.LoginUser;

@Repository
public interface LoginUserRepository extends CrudRepository<LoginUser, String>{

}
