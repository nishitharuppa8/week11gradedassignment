package com.hcl.admin.service;

import com.hcl.admin.bean.LoginUser;

public interface ILoginUserService {

	public boolean registerUser(LoginUser loginUser);
	public LoginUser findUser(LoginUser loginUser) throws Exception;
}
