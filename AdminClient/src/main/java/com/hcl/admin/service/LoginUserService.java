package com.hcl.admin.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.admin.bean.LoginUser;
import com.hcl.admin.repository.LoginUserRepository;

@Service
public class LoginUserService implements ILoginUserService {

	@Autowired
	private LoginUserRepository loginUserRepository;

	@Override
	public boolean registerUser(LoginUser loginUser) {
		System.out.println(loginUser.getEmailId());
		if(this.loginUserRepository.existsById(loginUser.getEmailId())) {
			return false;
		}
		this.loginUserRepository.save(loginUser);
		return true;
	}

	@Override
	public LoginUser findUser(LoginUser loginUser) throws Exception {
		System.out.println("LoginUser is " + loginUser);
		System.out.println(this.loginUserRepository.findById(loginUser.getEmailId()).get());
		return this.loginUserRepository.findById(loginUser.getEmailId()).get();
	}
}
