<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Book Management</title>
<style type="text/css">
body {
        padding: 0px;
        margin: 0;
        font-family: Verdana, Geneva, Tahoma, sans-serif;
    } 
#header {
        background-color: #16a085;
        color: #fff;
    }
h1 {
        font-weight: 600;
        text-align: center;
        background-color: #16a085;
        color: #fff;
        padding: 10px 0px;
    }              
</style>
</head>
<body>
	<%
		String email = (String)session.getAttribute("email");
		String name = (String) session.getAttribute("name");
		String type = (String) session.getAttribute("type");
	%>
	<div class="header" id="headerId" align="center">
		<h1>BOOK STORE</h1>
		<hr>
		<div id="welcome" align="center">
		
			Welcome Mr/Mrs. <strong class="name" title="Your Name"> <%=name%>
			</strong>
		
	</div>
	<hr>
		<div class="navLink" align="center">
		

				<% if(email != null && type.equals("user")){ %>
				<a href="dashboard"><button
							class="btn btn-success">Dashboard</button></a>
				
				<a href="logout" class="login"><button
							class="btn btn-primary">Log out</button></a><hr>

				<%}else if(email != null && type.equals("admin")){%>

				<a href="dashboard"><button
							class="btn btn-success">Dashboard</button></a>
				
				<a href="logout" class="login"><button
							class="btn btn-primary">Log out</button></a>
<hr>
<h3>Open User Server & Check books/Add Books/Retrive.</h3>
				<%} %>
			
		</div>
	</div>
	
</body>
</html>