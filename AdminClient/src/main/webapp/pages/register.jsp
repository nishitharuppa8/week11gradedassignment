<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Book Management</title>
<style type="text/css">
body {
        padding: 0px;
        margin: 0;
        font-family: Verdana, Geneva, Tahoma, sans-serif;
    } 
#header {
        background-color: #16a085;
        color: #fff;
    }
h1 {
        font-weight: 600;
        text-align: center;
        background-color: #16a085;
        color: #fff;
        padding: 10px 0px;
    }              
</style>

<body>
	<div class="header" id="headerId" align="center">
		<h1>BOOK STORE</h1>
		<hr>
		

				<a href="login" class="login"><button
							class="btn btn-primary">Login</button></a>
				<a href="register" class="login"><button
							class="btn btn-primary">Register</button></a>
			
		<hr>
	</div>


	<form action="getRegistered" method="post">
		<div class="cardLogin" align="center">
		<h2>REGISTRATION</h2>
		<br>
			
			<div class="form-group userName">
				<label for="email">Email:</label> <input type="text"
					class="form-control inp" id="email" placeholder="Enter email"
					name="email">
			</div>
<br>
			<div class="form-group userName">
				<label for="name">Name:</label> <input type="text"
					class="form-control inp" id="name" placeholder="Enter Name"
					name="name">
			</div>
<br>
			<div class="form-group userName">
				<label for="password">Password:</label> <input type="password"
					class="form-control inp" id="email" placeholder="Enter Password"
					name="password">
			</div>
<br>
			<div align="center" class="type">
				<p>Type :</p>
				<label for="admin" class="radiobtn">Admin</label>   <input
					type="radio" id="admin" name="type" value="admin" class="radiobtn">
				<label for="user" class="radiobtn">User</label>   <input
					type="radio" id="user" name="type" value="user" class="radiobtn">
			</div>
<br><br>
			<div align="center">
				<button type="submit" class="btn btn-success" value="Register">Register</button>
			</div>
		</div>
	</form>
	<hr>
</body>
</html>