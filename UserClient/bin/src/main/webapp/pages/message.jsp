<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Book Management</title>
<link
	
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">
</head>
<style type="text/css">
body {
        padding: 0px;
        margin: 0;
        font-family: Verdana, Geneva, Tahoma, sans-serif;
    } 
#header {
        background-color: #16a085;
        color: #fff;
    }
h1 {
        font-weight: 600;
        text-align: center;
        background-color: #16a085;
        color: #fff;
        padding: 10px 0px;
    }              
</style>
<body>
	<%
	String email = (String) session.getAttribute("email");
	String name = (String) session.getAttribute("name");
	String type = (String) session.getAttribute("type");
	String message = (String) session.getAttribute("mssg");
	%>
	<div class="header" id="headerId">
		<h1>BOOK STORE</h2>
		<hr>
		<div class="navLink">
			<ul class="navUL">
				<%
				if (email != null && type.equals("user")) {
				%><li class="li"><a href="/dashboard"><button
							class="btn btn-success">Dashboard</button></a></li>
				<li class="li"><a href="book" class="login"><button
							class="btn btn-primary">Show All Books</button></a></li>
				<li class="li"><a href="like" class="login"><button
							class="btn btn-primary">Liked Books</button></a></li>
				<li class="li"><a href="read" class="login"><button
							class="btn btn-primary">Read Later Books</button></a></li>
				<li class="li"><a href="logout" class="login"><button
							class="btn btn-primary">Log out</button></a></li>
				<%
				} else if (email != null && type.equals("admin")) {
				%>
				<li class="li"><a href="/dashboard"><button
							class="btn btn-success">Dashboard</button></a></li>
				<li class="li"><a href="book" class="login"><button
							class="btn btn-primary">Show All Books</button></a></li>
				<li class="li"><a href="add" class="login"><button
							class="btn btn-primary">Add Book</button></a></li>
				<li class="li"><a href="delete" class="login"><button
							class="btn btn-primary">Delete Book</button></a></li>
				<li class="li"><a href="logout" class="login"><button
							class="btn btn-primary">Log out</button></a></li>
				<%
				} else {
				%>
				<div align="center">
				<h3>Login Here</h3>
				<a href="login"	class="login"><button class="btn btn-primary">Login</button></a></li>
				</div>
				<hr>
				<%
				}
				%>
			</ul>
		</div>
	</div>
	<%
	if (email != null) {
	%>
	<div id="welcome">
		<br>
		<br>
		<br>
		<br>
		
		<strong class="name" title="Your Name"> 
			</strong>
		
	</div>
	<%
	} else {
	%>
	<div id="welcome">
		<br>
		<br>
		<br>
		<br>
		
			 <strong class="name" title="Your Name"> 
			</strong>
		
	</div>
	<%}%>
	

</html>