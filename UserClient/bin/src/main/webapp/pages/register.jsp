<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Book Management</title>
<link

	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">

<style type="text/css">
body {
        padding: 0px;
        margin: 0;
        font-family: Verdana, Geneva, Tahoma, sans-serif;
    } 
#header {
        background-color: #16a085;
        color: #fff;
    }
h1 {
        font-weight: 600;
        text-align: center;
        background-color: #16a085;
        color: #fff;
        padding: 10px 0px;
    }              
</style>
</head>
<body>
	<div id="headerId">
	<hr>
		<div align="center"><h1>Book Library Management</h1></div>
		<hr>
		<div align="center">


				<a href="login" class="login"><button
							class="btn btn-primary">Login</button></a>
	<hr>
		</div>
	</div>

<div align="center">
	<form action="getRegistered" method="post">
		<div class="cardLogin">
			<h2>Registration</h2>
			<div class="form-group userName">
				<label for="email">Email</label> <input type="text"
					class="form-control inp" id="email" placeholder="Enter email"
					name="email">
			</div>
<br>
			<div class="form-group userName">
				<label for="name">Name</label> <input type="text"
					class="form-control inp" id="name" placeholder="Enter Name"
					name="name">
			</div>
<br>
			<div class="form-group userName">
				<label for="password">Password</label> <input type="password"
					class="form-control inp" id="email" placeholder="Enter Password"
					name="password">
			</div>

			<div align="center" class="type">
			<h4>Type</h4>
				<label for="admin" class="radiobtn">Admin</label>   <input
					type="radio" id="admin" name="type" value="admin" class="radiobtn">
				<label for="user" class="radiobtn">User</label>   <input
					type="radio" id="user" name="type" value="user" class="radiobtn">
			</div>
<br>
			<div align="center">
				<button type="submit" class="btn btn-success" value="Register">Submit</button>
			</div>
		</div>
	
	</form>
	</div>
	<hr>
</body>
</html>