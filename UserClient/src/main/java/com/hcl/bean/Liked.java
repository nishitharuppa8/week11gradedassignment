package com.hcl.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Liked {

	@Id
	private int bookId;
	private String bookName;
	private String bookGenre;
	private String emailId;
	private String bookAuthor;
	private String bookYear;

	public Liked() {

	}

	public int getBookId() {
		return bookId;
	}

	public void setBookId(int bookId) {
		this.bookId = bookId;
	}

	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	public String getBookGenre() {
		return bookGenre;
	}

	public void setBookGenre(String bookGenre) {
		this.bookGenre = bookGenre;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmail(String emailId) {
		this.emailId = emailId;
	}

	public String getBookAuthor() {
		return bookAuthor;
	}

	public void setBookAuthor(String bookAuthor) {
		this.bookAuthor = bookAuthor;
	}

	public String getBookYear() {
		return bookYear;
	}

	public void setBookYear(String bookYear) {
		this.bookYear = bookYear;
	}

	@Override
	public String toString() {
		return "Liked [bookId=" + bookId + ", bookName=" + bookName + ", bookGenre=" + bookGenre + ", emailId=" + emailId
				+ ", bookAuthor=" + bookAuthor + ", bookYear=" + bookYear + "]";
	}

	public Liked(int bookId, String bookName, String bookGenre, String emailId, String bookAuthor, String bookYear) {
		super();
		this.bookId = bookId;
		this.bookName = bookName;
		this.bookGenre = bookGenre;
		this.emailId = emailId;
		this.bookAuthor = bookAuthor;
		this.bookYear = bookYear;
	}



}

