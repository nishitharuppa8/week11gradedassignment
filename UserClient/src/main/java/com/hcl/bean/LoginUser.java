package com.hcl.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class LoginUser {

	@Id
	private String email;
	private String name;
	private String password;
	private String type;

	public LoginUser() {
		System.out.println("Login User Class Constructor");
	}

	public LoginUser(String email, String name, String password,String type) {
		super();
		this.email = email;
		this.name = name;
		this.password = password;
		this.type = type;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "LoginUser [email=" + email + ", name=" + name + ", password=" + password + ", type=" + type + "]";
	}
}
