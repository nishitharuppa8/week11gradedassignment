package com.hcl.controllers;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.hcl.bean.Book;
import com.hcl.bean.Liked;
import com.hcl.service.BookService;
import com.hcl.service.LikedBookService;

@Controller
public class LikedController {

	@Autowired
	private LikedBookService likedBookService;

	@Autowired
	private BookService bookService;

	@GetMapping("/like")
	public String getLikedBooks(Map<String, List<Liked>> map,HttpSession httpSession){
		List<Liked> listOfLikedBoooks = this.likedBookService.getAllLikedBooks();

		map.put("listOfLikedBoooks", listOfLikedBoooks);

		if(listOfLikedBoooks.isEmpty()) {
			httpSession.setAttribute("msg", "Liked Books List is Empty");
			return "redirect:/message";
		}else {
			for(Liked liked : listOfLikedBoooks) {
				System.out.println(liked);
			}
			return "liked";
		}
	}

	@RequestMapping(value = "/addliked/{id}",method = RequestMethod.GET)
	public String getBookById(@PathVariable int id,HttpSession httpSession) {
		Book book = this.bookService.getBook(id);
		if(book == null) {
			System.out.println("getBook returns null book "+book);
			httpSession.setAttribute("msg", "Book Id Does not Exists");
			return "redirect:/message";
		}else {
			Liked liked = new Liked(id, book.getBookName(), book.getBookGenre(),(String)httpSession.getAttribute("email"),book.getBookAuthor(),book.getBookYear());
			boolean isAdded = this.likedBookService.addToLiked(liked);
			if(isAdded){
				System.out.println("Book Added to Liked");
				httpSession.setAttribute("msg", "Book Added to Liked");
				return "redirect:/message";
			}else {
				httpSession.setAttribute("msg", "Book is already Present is the Liked Section");
				return "redirect:/message";
			}
		}
	}


	@GetMapping(value = "/removeliked/{id}")
	public String deleteById(@PathVariable int id,HttpSession httpSession){
		boolean isDeleted = this.likedBookService.deleteBook(id);

		if(isDeleted) {
			System.out.println("Book is deleted from liked");
			httpSession.setAttribute("msg", "Book with id " + id + " Removed From Liked");
			return "redirect:/message";
		}else {
			return "dashboard";
		}
	}

}
