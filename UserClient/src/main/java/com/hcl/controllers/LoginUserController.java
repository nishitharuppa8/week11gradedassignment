package com.hcl.controllers;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.hcl.bean.LoginUser;
import com.hcl.service.LoginUserService;

@Controller
public class LoginUserController {

	@Autowired
	private LoginUserService loginUserService;

	@PostMapping("/getRegistered")
	public String getUserRegistered(LoginUser loginUser,HttpSession httpSession) {

		System.out.println("getRegisterDashboardPage is called with " + loginUser);

		if(this.loginUserService.registerUser(loginUser)) {
			System.out.println(this.loginUserService.registerUser(loginUser));
			System.out.println("User is Registered");
			httpSession.setAttribute("msg", "Your Are Registered Successfully Please Login");
			return "redirect:/message";
		}else {
			httpSession.setAttribute("msg", "You are Already Registered");
			return "redirect:/message";
		}
	}

	@GetMapping("/login")
	public String getLoginPage() {
		return "login";
	}
	@GetMapping("/dashboard")
	public String getDashboard() {
		return "dashboard";
	}


	@PostMapping("/dashboard")
	public String getDashboard(LoginUser loginUser,HttpSession httpSession) {
		LoginUser user;
		try {
			user = this.loginUserService.findUser(loginUser);

			if(user.getEmail().equals(loginUser.getEmail()) && user.getPassword().equals(loginUser.getPassword())) {
				httpSession.setAttribute("email",user.getEmail());
				httpSession.setAttribute("name", user.getName());
				httpSession.setAttribute("type", user.getType());
				return "dashboard";
			}else {
				System.out.println("Not a valid Password");
				httpSession.setAttribute("msg", "Not a valid Password");
				return "redirect:/message";
			}
		} catch (Exception e) {
			e.printStackTrace();
			httpSession.setAttribute("mssg", "You Are Not A Registered User Please Register");
			return "redirect:/message";
		}
	}

	@GetMapping("/register")
	public String getRegisterPage() {
		return "register";
	}

	@GetMapping("/logout")
	public String logout(HttpSession session) {
		session.removeAttribute("email");
		session.removeAttribute("name");
		session.removeAttribute("type");
		session.invalidate();
		return "redirect:/login";
	}


}