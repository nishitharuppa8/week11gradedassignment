package com.hcl.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.hcl.bean.Liked;

@Repository
public interface LikedRepository extends CrudRepository<Liked, Integer>{

}
