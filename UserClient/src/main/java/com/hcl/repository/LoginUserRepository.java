package com.hcl.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.hcl.bean.LoginUser;

@Repository
public interface LoginUserRepository extends CrudRepository<LoginUser, String>{
	
	

}
