package com.hcl.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.hcl.bean.ReadLater;

@Repository
public interface ReadLaterRepository extends CrudRepository<ReadLater, Integer>{

}
