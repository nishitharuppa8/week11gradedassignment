<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Book Management</title>
<style type="text/css">
body {
        padding: 0px;
        margin: 0;
        font-family: Verdana, Geneva, Tahoma, sans-serif;
    } 
#header {
        background-color: #16a085;
        color: #fff;
    }
h1 {
        font-weight: 600;
        text-align: center;
        background-color: #16a085;
        color: #fff;
        padding: 10px 0px;
    }              
</style>
<body>
	<%
	String email = (String) session.getAttribute("email");
	String name = (String) session.getAttribute("name");
	%>
	<div class="header" id="headerId">
		<h1>BOOK STORE</h1>
		<hr>
		<div id="welcome" align="center">
			
			Welcome Mr/Mrs. <strong class="name" title="Your Name"> <%=name%>
			</strong>
		<hr>
	</div>
		<div align="center">
			
				<%
				if (email != null) {
				%>
				<a href="/dashboard"><button
							class="btn btn-success">Dashboard</button></a>
				<a href="book" class="login"><button
							class="btn btn-primary">Show Books</button></a>
				<a href="logout" class="login"><button
							class="btn btn-primary">Log out</button></a>
				<%}%>
		<hr>
		</div>
	</div>
	
<div align="center">
	<form action="addbook" method="POST">
		<div class="cardLogin">
			<h2>ADD BOOK DETAILS</h2>
			<br>
			<div class="form-group userName">
				<label for="name" class="lab">Book Name</label> <input type="text"
					class="form-control inp" id="name" placeholder="Enter Book Name"
					name="bookName">
			</div>
			<br>
			<div class="form-group userName">
				<label for="genre" class="lab">Book Genre:</label> <input
					type="text" class="form-control inp" id="genre"
					placeholder="Enter Book genre" name="bookGenre">
			</div>
<br>
			<div class="form-group userName">
				<label for="Author" class="lab">Book Author:</label> <input
					type="text" class="form-control inp" id="author"
					placeholder="Enter Book Author Name" name="bookAuthor">
			</div>
<br>
			<div class="form-group userName">
				<label for="year" class="lab">Book Year:</label> <input type="text"
					class="form-control inp" id="year" placeholder="Enter year"
					name="bookYear">
			</div>
<br>
			<div align="center">
				<button type="submit" class="btn btn-success" value="Login">Add
					Book</button>
					<br>
					<hr>
			</div>
		</div>
	</form>
	</div>
</body>
</html>