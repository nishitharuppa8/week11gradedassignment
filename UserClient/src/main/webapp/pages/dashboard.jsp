<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Book Store</title>
<style type="text/css">
body {
        padding: 0px;
        margin: 0;
        font-family: Verdana, Geneva, Tahoma, sans-serif;
    } 
#header {
        background-color: #16a085;
        color: #fff;
    }
h1 {
        font-weight: 600;
        text-align: center;
        background-color: #16a085;
        color: #fff;
        padding: 10px 0px;
    }              
</style>
</head>
<body>
	<%
		String email = (String)session.getAttribute("email");
		String name = (String) session.getAttribute("name");
		String type = (String) session.getAttribute("type");
	%>
	<div class="header" id="headerId">
		<h1>BOOK STORE</h1>
		<hr>
		<div id="welcome" align="center">
		
			Welcome Mr/Mrs. <strong class="name" title="Your Name"> <%=name%>
			</strong>
			</div>
			<hr>
		<div class="navLink">
			

				<% if(email != null && type.equals("user")){ %>
				<div align="center">
				
				<a href="book" class="login"><button
							class="btn btn-primary">Show Books</button></a>
				<a href="like" class="login"><button
							class="btn btn-primary">Liked Books</button></a>
				<a href="read" class="login"><button
							class="btn btn-primary">Read Later Books</button></a>
				<a href="logout" class="login"><button
							class="btn btn-primary">Log out</button></a>
							<hr>
							</div>

				<%}else if(email != null && type.equals("admin")){%>
<div align="center">
				
				<a href="book" class="login"><button
							class="btn btn-primary">Show Books</button></a>
				<a href="add" class="login"><button
							class="btn btn-primary">Add Book</button></a>
				<a href="delete" class="login"><button
							class="btn btn-primary">Delete Book</button></a>
				<a href="logout" class="login"><button
							class="btn btn-primary">Log out</button></a>
							<hr>
							</div>

				<%} %>
			</ul>
		</div>
	</div>
	
</body>
</html>